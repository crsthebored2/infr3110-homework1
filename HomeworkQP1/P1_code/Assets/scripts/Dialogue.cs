﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour {
    static public List<Script> script = new List<Script>();
    static public int currentline;
    public Text output;

    NodeEditor nodeEditor;
    


	// Use this for initialization
	void Start () {
        nodeEditor = new NodeEditor();
        nodeEditor.Show();
        Load("Assets/res/dialogue.txt");

        for(int i = 0; i < script.Count; i++)
        {
            nodeEditor.createNode(i, script[i]);
        }       


        print("Test get num: " + getNum().ToString());
        print("Test get line: " + getLine(3));
        print("Test get name: " + getCharacterName(2));
    }

    public bool Load(string fileName)
    {
        try
        {
            string line;
            StreamReader theReader = new StreamReader(fileName);
  
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    line = theReader.ReadLine();
                    if (line != null)
                    {
                        string[] entries = line.Split(',');
                        if (entries.Length > 0)
                        {
                            int _ID = Convert.ToInt32(entries[0]);
                            int _nextID = Convert.ToInt32(entries[1]);
                            string _type = entries[2];
                            string _name = entries[3];
                            string _line = entries[4];

                            print(_type);


                            Script newScript = new Script(_ID, _nextID, _type, _name, _line);
                            script.Add(newScript);                                 
                        }
                    }
                }
                while (line != null); 
                theReader.Close();
                return true;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("{0}\n", e.Message);
            return false;
        }
    }

    int getNum()
    {
        int numLines = 0;
        for (int i = 0; i < script.Count; i++)
            if (script[i].getType() == "line")
                numLines++;

        return numLines;
    }

    string getLine(int _ID)
    {
        for (int i = 0; i < script.Count; i++)
            if (script[i].getID() == _ID)
                return script[i].getDialogue();

        return "Couldn't find line associated with given ID";    
    }

    string getCharacterName(int _ID)
    {
        for (int i = 0; i < script.Count; i++)
            if (script[i].getID() == _ID)
                return script[i].getName();

        return "Couldn't find line associated with given ID";
    }

    // Update is called once per frame
    void Update () {
        if (script[currentline].getType() == "Line")
        {
            output.text = script[currentline].getDialogue();
            if (Input.GetKeyUp(KeyCode.Space))
            {
                currentline = script[currentline].getNextID()-1;
                if (currentline >= script.Count)
                    currentline = 0;
            }
        }
        else if (script[currentline].getType() == "Option2")
        {
            print("option line");
            output.text = script[currentline].getName() + "       " + script[currentline].getDialogue();
            if (Input.GetKeyUp("1"))
            {
                currentline = script[currentline].getNextO1() - 1;
                if (currentline >= script.Count)
                    currentline = 0;
            }
            else if (Input.GetKeyUp("2"))
            {
                currentline = script[currentline].getNextO2() - 1;
                if (currentline >= script.Count)
                    currentline = 0;
            }
        }
	}





}


public class Script
{
    int ID;
    int nextID;
    string type;
    string name;
    string dialogue;

    int o1NextID;
    int o2NextID;


    public Script(int _ID, int _nextID, string _type, string _name, string _dialogue)
    {
        ID = _ID;
        nextID = _nextID;
        type = _type;
        name = _name;
        dialogue = _dialogue;

        //only used on option lines
        o1NextID = nextID;
        o2NextID = nextID;

        if (type == "Option2")
        {
            string[] option1 = name.Split(';');
            if (option1.Length > 1)
            {
                name = option1[0];
                o1NextID = Convert.ToInt32(option1[1]);
            }

            string [] option2 = dialogue.Split(';');
            if (option2.Length > 1)
            {
                dialogue = option2[0];
                o2NextID = Convert.ToInt32(option2[1]);
            }
        }
    }

    public int getID()
    { return ID; }

    public int getNextID()
    { return nextID; }

    public string getType()
    { return type; }

    public string getName()
    { return name; }

    public string getDialogue()
    { return dialogue; }

    public int getNextO1()
    { return o1NextID; }

    public int getNextO2()
    { return o2NextID; }

}


public class NodeEditor : EditorWindow
{
    public List<Rect> windows = new List<Rect>();
    List<int> windowsToAttach = new List<int>();
    List<int> attachedWindows = new List<int>();
    int posX= 10;

    List<Script> data = new List<Script>();

    [MenuItem("Window/Node editor")]
    static void ShowEditor()
    {
        NodeEditor editor = EditorWindow.GetWindow<NodeEditor>();
    }


    void OnGUI()
    {
        if (windowsToAttach.Count == 2)
        {
            attachedWindows.Add(windowsToAttach[0]);
            attachedWindows.Add(windowsToAttach[1]);
            windowsToAttach = new List<int>();
        }

        if (attachedWindows.Count >= 2)
        {
            for (int i = 0; i < attachedWindows.Count; i += 2)
            {
                DrawNodeCurve(windows[attachedWindows[i]], windows[attachedWindows[i + 1]]);
            }
        }




        BeginWindows();

/*        if (GUILayout.Button("Create Node"))
        {
            windows.Add(new Rect(10, 10, 100, 100));
        }
*/        

        for (int i = 0; i < windows.Count; i++)
        {
            windows[i] = GUI.Window(i, windows[i], DrawNodeWindow, "Window " + i);
        }

        EndWindows();
    }

    public void createNode(int id, Script _data)
    {
        Script newScript = new Script(_data.getID(), _data.getNextID(), _data.getType(), _data.getName(), _data.getDialogue());
        data.Add(newScript);
        windows.Add(new Rect(posX, 10, 100, 200));

        posX += 110;

       windowsToAttach.Add(id);
    }


    void DrawNodeWindow(int id)
    {

        GUILayout.Label(data[id].getID().ToString());
        GUILayout.Label(data[id].getNextID().ToString());
        GUILayout.Label(data[id].getType());
        GUILayout.Label(data[id].getName());
        GUILayout.Label(data[id].getDialogue());

        GUI.DragWindow();
    }




    void DrawNodeCurve(Rect start, Rect end)
    {
        Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
        Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
        Vector3 startTan = startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left * 50;
        Color shadowCol = new Color(0, 0, 0, 0.06f);

        for (int i = 0; i < 3; i++)
        {// Draw a shadow
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
        }

        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
    }

    public void Update()
    {
        // This will only get called 10 times per second.
        Repaint();
    }




}