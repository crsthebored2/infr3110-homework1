﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public Camera mainCam, nadCam, natCam;

	// Use this for initialization
	void Start () {
        mainCam.enabled = true;
        natCam.enabled = false;
        nadCam.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	    if (Dialogue.script[Dialogue.currentline].getName() == "Nadine")
        {
            nadCam.enabled = true;
            mainCam.enabled = false;
            natCam.enabled = false;
        }
        else if (Dialogue.script[Dialogue.currentline].getName() == "Nate")
        {
            natCam.enabled = true;
            mainCam.enabled = false;
            nadCam.enabled = false;
        }
        else
        {
            mainCam.enabled = true;
            natCam.enabled = false;
            nadCam.enabled = false;
        }
    }
}
